import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoatanComponent } from './roatan.component';

describe('RoatanComponent', () => {
  let component: RoatanComponent;
  let fixture: ComponentFixture<RoatanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoatanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoatanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
