import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComayaguaComponent } from './comayagua.component';

describe('ComayaguaComponent', () => {
  let component: ComayaguaComponent;
  let fixture: ComponentFixture<ComayaguaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComayaguaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComayaguaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
