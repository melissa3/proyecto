import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopanComponent } from './copan.component';

describe('CopanComponent', () => {
  let component: CopanComponent;
  let fixture: ComponentFixture<CopanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
