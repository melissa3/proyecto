import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AppRoutingModule,routingComponent} from './app-routing.module';
import {AppComponent} from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { AmapalaComponent } from './amapala/amapala.component';
import { ComayaguaComponent } from './comayagua/comayagua.component';
import { RoatanComponent } from './roatan/roatan.component';
import { SantaComponent } from './santa/santa.component';
import { TelaComponent } from './tela/tela.component';
import { PuertoComponent } from './puerto/puerto.component';
import { CeibaComponent } from './ceiba/ceiba.component';
import { FranciscoComponent } from './francisco/francisco.component';
import { CopanComponent } from './copan/copan.component';
import { NotFoundComponent } from './not-found/not-found.component';


@NgModule({
  declarations: [
    AppComponent,
    routingComponent,
    InicioComponent,
    AmapalaComponent,
    ComayaguaComponent,
    RoatanComponent,
    SantaComponent,
    TelaComponent,
    PuertoComponent,
    CeibaComponent,
    FranciscoComponent,
    CopanComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
