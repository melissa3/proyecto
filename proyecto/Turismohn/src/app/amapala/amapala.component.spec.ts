import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmapalaComponent } from './amapala.component';

describe('AmapalaComponent', () => {
  let component: AmapalaComponent;
  let fixture: ComponentFixture<AmapalaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmapalaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmapalaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
