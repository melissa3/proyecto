import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { HotelesComponent } from './hoteles/hoteles.component';
import { LugaresComponent } from './lugares/lugares.component';
import { ContactoComponent } from './contacto/contacto.component';
import { AmapalaComponent } from './amapala/amapala.component';
import { CeibaComponent } from './ceiba/ceiba.component';
import { ComayaguaComponent } from './comayagua/comayagua.component';
import { CopanComponent } from './copan/copan.component';
import { FranciscoComponent } from './francisco/francisco.component';
import { RoatanComponent } from './roatan/roatan.component';
import { SantaComponent } from './santa/santa.component';
import { TelaComponent } from './tela/tela.component';
import { PuertoComponent } from './puerto/puerto.component';


const routes: Routes = [
 // {path:"lugar", component:lugaresComponent}
 {
  path: '',
  redirectTo: '/inicio',
  pathMatch: 'full'
},
{path:"inicio", component:InicioComponent},
{path:"contacto", component:ContactoComponent},
{path:"hoteles", component:HotelesComponent},
{path:"lugares", component:LugaresComponent},
{path:"amapala", component:AmapalaComponent},
{path:"ceiba", component: CeibaComponent},
{path:"comayagua", component: ComayaguaComponent},
{path:"copan", component: CopanComponent},
{path:"francisco", component: FranciscoComponent},
{path:"roatan", component: RoatanComponent},
{path:"santa", component: SantaComponent},
{path:"tela", component: TelaComponent},
{path:"puerto", component: PuertoComponent},

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent=[InicioComponent,ContactoComponent,HotelesComponent,LugaresComponent];

